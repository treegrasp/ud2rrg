# ud2rrg

Rule-based conversion from Universal Dependenices (UD) to Role and Reference Grammar (RRG)

This program is explained in Evang et al. (2021)

## Installation

1. Install [disco-dop](https://github.com/andreasvc/disco-dop).

2. Download VerbNet:

    wget http://verbs.colorado.edu/verb-index/vn/verbnet-3.2.tar.gz
    tar xf verbnet-3.2.tar.gz

3. Install additional dependencies:

    pip install conllu

## References

Kilian Evang, Tatiana Bladier, Laura Kallmeyer, Simon Petitjean (2021).
Bootstrapping Role and Reference Grammar Treebanks via Universal Dependencies.
In *Proceedings of the Proceedings of the Fifth Workshop on Universal
Dependencies (UDW)*.
